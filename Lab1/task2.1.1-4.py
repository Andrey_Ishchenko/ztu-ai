import numpy as np
from sklearn import preprocessing

input_data = np.array([[-5.3, -8.9, 3.0],
	                   [2.9, 5.1, -3.3],
                       [3.1, -2.8, -3.2],
                       [2.2, -1.4, 5.1]])

bin_data = preprocessing.Binarizer(threshold=3.0).transform(input_data)

# Task 2.1.1
print("Binarized data:\n", bin_data)

# Task 2.1.2
print("\n BEFORE:")
print("Mean = ", input_data.mean(axis=0))
print("Std deviation = ", input_data.std(axis=0))

data_scaled = preprocessing.scale(input_data)
print("\n AFTER:")
print("Mean = ", data_scaled.mean(axis=0))
print("Std deviation = ", data_scaled.std(axis=0))

# Task 2.1.3
data_scaler_minmax = preprocessing.MinMaxScaler(feature_range=(0,1)).fit_transform(input_data)
print("\n Min Max Scaled Data = \n", data_scaler_minmax)

# Task 2.1.4
data_normalized_l1 = preprocessing.normalize(input_data, norm='l1')
data_normalized_l2 = preprocessing.normalize(input_data, norm='l2')
print("\n l1 normalized data: \n", data_normalized_l1)
print("\n l2 normalized data: \n", data_normalized_l2)
