import numpy as np
import matplotlib.pyplot as plt
from sklearn.preprocessing import PolynomialFeatures
from sklearn.linear_model import LinearRegression

m = 100
X = 6 * np.random.rand(m, 1) - 5
y = 0.7 * X ** 2 + X + 3 + np.random.randn(m, 1)

poly_features = PolynomialFeatures(degree=2, include_bias=False)
X_poly = poly_features.fit_transform(X)

print('X[0]: ', X[0], '\n\n X_poly: \n', X_poly)

model = LinearRegression()
model.fit(X_poly, y)

X_new = np.linspace(-5, 2, 100).reshape(-1, 1)
X_new_poly = poly_features.transform(X_new)
y_new = model.predict(X_new_poly)

print("Coef: ", list(map(lambda n: np.round(n, 2), model.coef_)), '')
print("Intercept: ", np.round(model.intercept_, 2))


plt.scatter(X, y, label='Original Data')
plt.plot(X_new, y_new, 'r-', label='Polynomial Regression', linewidth=2)
plt.xlabel('X')
plt.ylabel('Y')
plt.legend()
plt.title('Polynomial Regression Example')
plt.show()
