import numpy as np
import matplotlib.pyplot as plt
from sklearn.metrics import mean_squared_error
from sklearn.model_selection import train_test_split
from sklearn.linear_model import LinearRegression
from sklearn.preprocessing import PolynomialFeatures
from sklearn.pipeline import Pipeline

def plot_learning_curves (model, x, y):
    X_train, X_val, y_train, y_val = train_test_split (x, y, test_size=0.2)
    train_errors, val_errors = [], []
    for m in range (1, len (X_train)):
        model.fit (X_train [:m], y_train [:m])
        y_train_predict = model.predict (X_train[:m])
        y_val_predict = model.predict (X_val)
        train_errors.append(mean_squared_error (y_train_predict, y_train[:m]))
        val_errors.append (mean_squared_error (y_val_predict, y_val))
    plt.plot(np. sqrt (train_errors), "r-+", linewidth=2, label="train")
    plt.plot (np.sqrt(val_errors), "b-", linewidth=3, label="val")
    plt.ylim(0, 6)
    plt.show()

X = 6 * np.random.rand(100, 1) - 5
y = 0.7 * X ** 2 + X + 3 + np.random.randn(100, 1)

reg = LinearRegression()
plot_learning_curves(reg, X, y)

polynomial_regression = Pipeline ([
    ("poly_features", PolynomialFeatures(degree=10, include_bias=False)),
    ("lin_reg", LinearRegression()),
])

plot_learning_curves (polynomial_regression, X, y)

polynomial_regression = Pipeline ([
    ("poly_features", PolynomialFeatures(degree=2, include_bias=False)),
    ("lin_reg", LinearRegression()),
])

plot_learning_curves (polynomial_regression, X, y)
